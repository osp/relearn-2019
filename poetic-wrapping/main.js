function wrapWords(str, tmpl) {
  return str.replace(/\w+/g, tmpl || "<tmp>$&</tmp>");
}

function unwrapWords(str, tmpl) {
  return str.replace(/<\/?tmp[^>]*>/g,"");
}

//wrapWords(str);

var elt = document.querySelector("q");
console.log(elt);
elt.innerHTML = wrapWords(elt.innerHTML);

var lastTop = 0;
var spans = document.getElementsByTagName('tmp');
var l = spans.length;
for (var i=0;i<l;i++) {
    var currentTop = spans[i].offsetTop;
    console.log(currentTop);

    if (!lastTop) { lastTop = currentTop; }

    else if (currentTop != lastTop) {
        //spans[i].classList.add("new");
        spans[i].innerHTML = "<span>«&#8239;</span>" + spans[i].innerHTML
        lastTop = currentTop;
    }
}

elt.innerHTML = unwrapWords(elt.innerHTML);
