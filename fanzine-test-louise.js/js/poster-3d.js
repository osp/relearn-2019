


let scene = new THREE.Scene(),
    renderer = window.WebGLRenderingContext ? new THREE.WebGLRenderer() : new THREE.CanvasRenderer(),
    light = new THREE.AmbientLight(0xffffff),
    camera,
    box;

const initScene = () => {
    scene.background = new THREE.Color(0xffffff);
    renderer.setSize(500, 500);
    renderer.setClearColor(0x000000, 0)

    document.getElementById('poster-pink').appendChild(renderer.domElement);
    scene.add(light)

    camera = new THREE.PerspectiveCamera(35, 500 / 500, 1, 1000)
    camera.position.set(85, 20, 85);
    camera.lookAt(scene.position)
    scene.add(camera)

    let boxMaterial = new THREE.MeshBasicMaterial({
        map: new THREE.TextureLoader().load("../assets/images/posters/ppdraw.jpg")
    })


    boxMaterial.color.set(0xffffff);
    box = new THREE.Mesh(
        new THREE.CubeGeometry(35, 55, 35),
        boxMaterial,
    )

    
    boxMaterial 
    // box.rotation.y = 40;
    // box.rotation.x = 90;
    // box.rotation.z = 60;
    box.name = "box";
    
    scene.add(box)




    // controls = new THREE.OrbitControls(camera)
    // controls.addEventListener('change', render)

    render();

}

const render = () => {
    box.rotation.y += 0.01
    renderer.render(scene, camera);
    requestAnimationFrame(render);
}

window.onload = initScene;





