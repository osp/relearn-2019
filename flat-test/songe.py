from flat import font, text, strike, document, view
import codecs

def layout(author, title, paragraphs):
    fonte = font.open('EBGaramond-Regular.ttf')
    body = strike(fonte).size(12, 16)
    headline = strike(fonte).size(25, 16)
    pieces = [
        body.paragraph(author),
        headline.paragraph(title),
        body.paragraph('')]
    pieces.extend(body.paragraph(p) for p in paragraphs)
    doc = document(140, 210, 'mm')
    page = doc.addpage()
    block = page.place(text(pieces))

    nn = 0 
    folio = 1
    while block.frame(18, 10, 50, 167).overflow():
        if nn == 1:
            block.frame(70, 20, 50, 160)
            page = doc.addpage() 
            nn = 0
        elif nn == 0:
            block.frame(10, 20, 50, 160)
            nn = 1
        block = page.chain(block)
        folio = folio + 1

    page = doc.addpage()

    return doc

fichier = codecs.open("songe.txt", "r", "utf-8")
lignes_brutes = fichier.readlines()
print(lignes_brutes)

fichier.close()

lignes = []

for l in lignes_brutes:
    lignes.append(l.replace("\n", "").replace("/"," /".replace(";", " ;")))

doc = layout('author', 'titre', lignes)
doc.pdf('layout.pdf')
