from flat import font, text, image, strike, document, view
import codecs

def layout(author, title, paragraphs):
    fonte = font.open('EBGaramond-Regular.ttf')
    body = strike(fonte).size(12, 16)
    headline = strike(fonte).size(25, 16)
    pieces = [
        body.paragraph(author),
        headline.paragraph(title),
        body.paragraph('')]
    pieces.extend(body.paragraph(p) for p in paragraphs)
    gravure1 = image.open(path='Hypnérotomachie_-_éd._Martin_-_p1v.jpg')

    doc = document(148, 210, 'mm')
    page = doc.addpage()
    block = page.place(text(pieces))
    block_gravure = page.place(gravure1)
    block_gravure.position(30,30)
    while block.frame(18, 21, 114, 167).overflow():
        page = doc.addpage()
        block = page.chain(block)
    return doc

fichier = codecs.open("songe.txt", "r", "utf-8")
lignes_brutes = fichier.readlines()
fichier.close()

lignes = []

for l in lignes_brutes:
    lignes.append(l.replace("\n", "").replace("/"," /".replace(";", " ;")))

doc = layout('Anonyme', 'Le Songe de Poliphile (éd. Popelin, 1883)', lignes)

doc.pdf('layout.pdf')
