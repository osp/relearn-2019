from flat import font, text, strike, document, view, image, group

def layout(title, *paragraphs):
    regular = font.open('Anaktoria_hint.ttf')
    bold = font.open('Anaktoria_hint.ttf')
    body = strike(regular).size(12, 16)
    headline = strike(bold).size(14, 16)
    fol = strike(bold).size(10, 20)
    notes = strike(bold).size(8, 10)
    illustration = image.open("en-tete.jpg")
    pieces1 = [
        headline.paragraph(title.center(20))
        ]
    pieces2 = [
        body.paragraph('')
        ]
    pieces2.extend(body.paragraph(p) for p in paragraphs)
    doc = document(148, 200, 'mm')
    page = doc.addpage()
    foliotage = page.place(fol.text(""))


    block = page.place(text(pieces1))
    block2 = page.place(text(pieces2))
    block_image = page.place(illustration)
    block_image.frame(10,10,170,30)
    block_image.fitwidth(85)
    block.frame(0,7,200,140)

    note = ""
    nbp = page.place(notes.text(" "))

    folio = 1

    n_column = 0

    gravure = image.open("gravure.jpg")

    while block.frame(15, 45, 50, 190).overflow():
        block.lines()
    while block2.frame(80, 70, 55, 115).overflow():
        if n_column == 1:
            block2.frame(73,70,55,115)
            foliotage = page.place(fol.text("%s" % folio))
            foliotage.frame(138, 187, 20, 20)
            page = doc.addpage()

            n_column = 0
            folio += 1
            if folio % 2 == 0:
                block2.frame(83,30,55,115)
            else:
                pass

            if folio == 2:
                note = "Cynthie et Cynthien, surnoms de Diane et d’Apollon, que Latone enfanta sur le mont Cynthus, dans l’île de Délos."
                nbp = page.place(notes.text("%s" % note))
                nbp.frame(100, 17, 30, 20)
            if folio == 3:
                note = "Monts que les anciens plaçaient dans la Scythie hyperboréenne."
                nbp = page.place(notes.text("%s" % note))
                nbp.frame(10, 17, 30, 20)
            if folio == 5:
                block_gravure = page.place(gravure)
                block_gravure.frame(10,10,170,30)
                block_gravure.fitwidth(60)
        elif n_column == 0:
            block2.frame(20,70,55,115)
            if folio % 2 == 0: # page impaire
                block2.frame(10,30,55,115)
                foliotage.frame(138, 7, 20, 20)
            else:
                foliotage.frame(10, 7, 20, 20)
            n_column = 1
        block2 = page.chain(block2)
    else:
        block2.frame(83,30,55,115)
        foliotage = page.place(fol.text("%s" % folio))
        foliotage.frame(183, 10, 20, 20)
        if folio % 2 == 0:
            block2.frame(83,30,55,115)
            foliotage.frame(138, 7, 20, 20)


    return doc

folio=""
doc = layout('HYPNÉROTOMACHIE DE POLIPHILE',
    ('Poliphile commence le récit de son Hypnérotomachie. Il décrit le temps et l’heure où, dans un songe, il lui sembla d’être sur une plage tranquille, silencieuse et inculte ; puis, de là, comment, sans y prendre garde, mais non sans une grande terreur, il se trouva dans une impénétrable et obscure forêt.'), '',
    ('Phœbus, à l’heure où resplendit le front de Matuta Leucothée, était déjà sorti des eaux de l’Océan ; il ne laissait pas apercevoir encore les roues suspendues et mobiles de son char, mais diligent, apparaissant à peine avec ses chevaux ailés Pyrois et Eous, il s’apprêtait à teindre en rose vermeil le quadrige blanchissant de sa fille que, rapide, il suivait. Déjà sa chevelure scintillante bouclait sur l’azur des flots mouvants. Il était à ce point du ciel où Cynthie[1], la non cornue, disparaissait en pressant ses deux chevaux, l’un blanc et l’autre noir, qui, ensemble avec le mulet de son véhicule, l’entraînaient à l’extrême horizon séparant les deux hémisphères où, mise en fuite, elle cédait le pas à la tremblante étoile messagère du jour.'), '',
    ('Alors les monts Riphées[2] étaient paisibles. Le glacial Eurus[3] ne venait plus, en gémissant sur leurs flancs, avec un souffle aussi âpre qu’en hiver, sous les cornes du Taureau lascif, secouer avec autant de violence les jeunes branches, ni tourmenter les joncs mobiles et pointus, non plus que les faibles cyprès, ni courber les osiers flexibles, ni agiter les saules languissants, ni incliner les sapins frêles. Orion lui-même, le hardi, ne poursuivait plus les sept Hyades[4] en pleurs. Alors les fleurs multicolores ne redoutaient pas la chaleur nuisible du fils d’Hypérion[5] qui s’avançait, mais, baignées des fraîches larmes de l’Aurore elles étaient tout humides de rosée ainsi que les prés verts. Les alcyons, sur les ondes unies et calmes de la mer apaisée, venaient construire leurs nids dans les sables du rivage.'), '',
    ('À l’heure donc où la plaintive Héro soupirait ardemment, parmi ces plages, après le départ douloureux du nageur Léander, moi, Poliphile, j’étais couché sur mon lit, secourable ami du corps fatigué ; personne des miens n’était dans ma chambre, si ce n’est ma chère et vigilante Agrypnie[6], laquelle, après m’avoir tenu des propos consolateurs — car je lui avais révélé la cause de mes profonds soupirs — me persuada de modérer mon trouble, et, s’avisant que l’heure de dormir était venue pour moi, prit congé. Demeuré seul, livré aux méditations intimes d’un amour unique, consumant sans sommeil la nuit longue et fastidieuse, inconsolable de ma Fortune ingrate, de mon étoile ennemie, pleurant sur ma passion malheureuse, j’examinais en tous points ce qu’est un amour sans réciprocité, cherchant comment il se peut faire qu’on aime précisément qui ne vous aime, et par quelle puissance l’âme abandonnée, assaillie par des attaques multiples, en proie à des combats violents, peut résister, faible comme elle est, surtout dans une lutte intérieure où elle demeure prise dans les mailles de pensées pressantes, instables et diverses.'), '',
    ('Je fus longtemps à me lamenter sur le fait de mon misérable état. L’esprit fatigué de vaines imaginations, repu d’un plaisir factice et décevant, je m’en prenais à un objet qui n’est pas mortel, qui est au contraire tout divin, à Polia dont l’idée vénérable m’occupe tout entier, vit en moi, y est profondément empreinte et gravée intimement. Déjà la lumière splendide des étoiles tremblotantes commençait à pâlir, lorsque ma langue cessa d’appeler cet ennemi désiré d’où procédait la grande bataille sans trève, cet oppresseur du cœur blessé qu’évoque toutefois celui-ci comme un remède efficace et profitable. Je réfléchissais sur la condition des amants malheureux résolus de mourir avec joie pour plaire à autrui et de vivre misérablement pour se complaire à eux-mêmes, ne nourrissant jamais leur vivant désir que d’une imagination vaine et pleine de soupirs.'), '',
    ('Cependant, ni plus ni moins qu’un homme brisé par les labeurs de la journée, ma plainte à peine apaisée, mes larmes taries à peine, tout pâle de la langueur d’amour, je me pris à souhaiter un repos opportun et naturel. Mes paupières rougies se fermèrent sur mes yeux humides et, sans être au juste ni dans une mort cruelle, ni dans une existence délectable, cette partie qui n’est pas unie aux esprits vigilants et amoureux et qui n’a que faire avec une opération aussi haute que la leur, se trouva envahie, dominée, vaincue par un long sommeil.'), '',
    ('Ô Jupiter altitonnant, heureux, admirable ! dirai-je cette vision inouïe, terrible, au point qu’en y pensant il n’est atome en tout mon être qui ne brûle et qui ne tremble ? Il me sembla d’être en une large plaine verdoyante, émaillée de mille fleurs et toute parée. Un silence absolu y régnait dans un air exquis. L’oreille la plus fine n’y percevait aucun bruit, aucun son de voix. La température y était adoucie par les rayons d’un soleil bienfaisant.'), '',
    ('Ici, me disais-je à part moi, tout rempli d’un étonnement craintif, aucune trace d’humanité n’apparaît à l’intuitif désir ; on n’y trouve aucune bête sauvage, aucun animal féroce ou domestique ; il n’y a pas une habitation rurale, il n’y a pas une hutte champêtre, pas un toit pastoral, pas une cabane. Dans ces sites herbins, on n’aperçoit aucun berger, on ne rencontre aucun banquet. Là, pas un pâtre de bœufs ou de cavales ; on n’y voit pas errer de troupeaux de moutons ou de gros bétail, accompagnés du flageolet rustique à deux trous, ou de la flûte sonore enveloppée d’écorce. Rempli de confiance par le calme de la plaine, par l’aménité du lieu, j’avançais rassuré, considérant de ci de là les jeunes frondaisons immobiles dans leur repos, ne discernant rien autre chose. Ainsi je dirigeai droit mes pas vers une épaisse forêt où, à peine entré, je m’avisai que, sans savoir comment, j’avais, sans prudence, perdu mon chemin. Voilà donc qu’une terreur subite envahit mon cœur en suspens et se répandit dans mes membres blêmes. Car il ne m’était pas donné de découvrir un moyen de sortie. Mais dans la forêt toute pleine de ronces et de broussailles, on ne voyait que scions touffus, qu’épines offensantes, que frênes sauvages hostiles aux vipères ; c’étaient des ormes rugueux amis des vignes fécondes, des lièges à la grosse écorce, qui tous s’enchevêtraient ; c’étaient des cerres massifs, des rouvres vigoureux, des chênes glandifères et des yeuses, aux rameaux si denses qu’ils ne laissaient pas filtrer jusque sur le sol humide les rayons du clair soleil, et que le dôme épais qu’ils formaient interceptait la lumière vivifiante. C’est ainsi que je me trouvai dans un air moite sous le couvert épais.'),'',
    ('Je commençais à pressentir et même à croire avec certitude que j’étais parvenu dans la vaste forêt Hercynienne[7]. On n’y rencontrait que tanières de bêtes féroces, antres d’animaux malfaisants et de fauves de la pire espèce. Aussi j’appréhendais, avec terreur, d’être, à l’improviste et sans défense, mis en pièces, comme Charydème[8], par la dent de quelque sanglier aux soies hérissées, ou par quelque auroch en furie, ou par quelque serpent sibilant, ou par des loups hurlants, que je voyais déjà me démembrer en dévorant mes chairs. Rempli de crainte, je me décidai, — secouant toute paresse, — à ne pas demeurer plus longtemps, à fuir le péril imminent, à presser mes pas incertains et désordonnés ; buttant à chaque instant contre les grosses racines à fleur du sol, allant à droite, allant à gauche, reculant, avançant, ne sachant où me diriger, et parvenant enfin dans un fourré inculte, lieu malsain et plein d’épines, où j’étais égratigné par les ronces et les pointes des prunelliers qui me déchiraient le visage. Les chardons aigus lacéraient mon vêtement et retardaient ma fuite en le retenant. En outre, je n’apercevais aucune piste, aucun sentier battu. Plein de défiance et d’inquiétude, je pressais mes pas d’autant, si bien que l’accélération de ma marche, le vent du sud et les mouvements de mon corps m’échauffèrent tellement, que ma poitrine glacée fut bientôt couverte d’une sueur abondante. Ne sachant plus que faire, mon esprit était tendu aux plus pénibles pensées. L’écho seul répondait à ma voix plaintive, et mes bruyants soupirs s’allaient perdre avec le cri rauque des cigales éprises de l’aurore et des grillons stridents. Enfin, dans ce bosquet impraticable et dangereux, je n’avais pour toute ressource que d’implorer la pitié secourable de la Crétoise Ariadne, qui, alors qu’elle consentit au meurtre du monstre fraternel[9], tendit le fil conducteur à l’enjôleur Thésée pour qu’il pût sortir de l’inextricable Labyrinthe. Et voilà où j’en étais réduit pour m’échapper de l’obscure forêt.'))

view(doc.pdf("songe.pdf"))
